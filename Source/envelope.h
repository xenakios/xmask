#pragma once

#include <memory>
#include <vector>
#include <algorithm>
#include <cmath>
#include "JuceHeader.h"

struct logistic_sequence
{
	logistic_sequence() {}
	double operator()()
	{
		double temp = m_x;
		m_x = m_x*(1.0 - m_x)*m_r;
		return temp;
	}
	double m_x = 0.4;
	double m_r = 3.991;
};

template<typename T>
inline T bound_value(T minval, T val, T maxval)
{
	if (val < minval)
		return minval;
	if (val > maxval)
		return maxval;
	return val;
}

enum class NodeShape
{
	Linear,
	Power,
	Abrupt
};

class EnvelopeNode
{
public:
	EnvelopeNode(double x, double y, NodeShape shape = NodeShape::Linear, double p1 = 0.0, double p2 = 0.0)
		: m_x(x), m_y(y), m_s(shape), m_p1(p1), m_p2(p2)
	{}
	double x() const { return m_x; }
	double y() const { return m_y; }
	NodeShape shape() const { return m_s; }
	double p1() const { return m_p1; };
	double p2() const { return m_p2; };
private:
	double m_x = 0.0;
	double m_y = 0.0;
	NodeShape m_s = NodeShape::Linear;
	double m_p1 = 0.0;
	double m_p2 = 0.0;
};

inline double get_shaped_value(double x, NodeShape sh, double p1, double)
{
	if (sh == NodeShape::Linear)
		return x;
	if (sh == NodeShape::Power)
	{
		if (p1 < 0.5)
		{
			double foo = 1.0 - (p1*2.0);
			return 1.0 - pow(1.0 - x, 1.0 + foo*4.0);
		}
		double foo = (p1 - 0.5)*2.0;
		return pow(x, 1.0 + foo*4.0);
	}
	return x;
}

class Envelope
{
public:
	Envelope()
	{
		m_name = "Unnamed envelope";
		m_colour = Colours::yellow;
	}
	void add_node(const EnvelopeNode& node)
	{
		m_nodes.push_back(node);
		sort_nodes();
	}
	size_t size() const { return m_nodes.size();  }
	const EnvelopeNode& get_const_node(size_t index) const
	{
		return m_nodes[index];
	}
	void set_node(size_t index, EnvelopeNode node) 
	{
		m_nodes[index]=node;
	}
	void remove(size_t index)
	{
		m_nodes.erase(m_nodes.begin() + index);
	}
	void sort_nodes()
	{
		std::stable_sort(m_nodes.begin(),m_nodes.end(),[](const EnvelopeNode& a, const EnvelopeNode& b)
		{
			return a.x() < b.x();
		});
	}
	double interpolate(double atime) const
	{
		int maxnodeind = m_nodes.size() - 1;
		if (m_nodes.size() == 0) return m_defvalue;
		if (m_nodes.size() == 1) return m_nodes[0].y();
		if (atime <= m_nodes[0].x())
			return m_nodes[0].y();
		if (atime>m_nodes[maxnodeind].x())
			return m_nodes[maxnodeind].y();
		const EnvelopeNode to_search(atime, 0.0);
		auto it = std::lower_bound(m_nodes.begin(), m_nodes.end(), to_search,
			[](const EnvelopeNode& a, const EnvelopeNode& b)
		{ return a.x()<b.x(); });
		if (it == m_nodes.end())
		{
			return m_defvalue;
		}
		--it; // lower_bound has returned iterator to point one too far
		double t1 = it->x();
		double v1 = it->y();
		double p1 = it->p1();
		double p2 = it->p2();
		NodeShape sh1 = it->shape();
		++it; // next envelope point
		double tdelta = it->x() - t1;
		if (tdelta<0.00001)
			tdelta = 0.00001;
		double vdelta = it->y() - v1;
		return v1 + vdelta*get_shaped_value(((1.0 / tdelta*(atime - t1))), sh1, p1, p2);
	}
	String m_name;
	Colour m_colour;
private:
	std::vector<EnvelopeNode> m_nodes;
	double m_defvalue = 0.5;
};

using EnvelopeRef = std::shared_ptr<Envelope>;

class TendencyMask
{
public:
	TendencyMask(String name, int pfield) : m_name(name), m_pfield(pfield)
	{
		auto env = std::make_shared<Envelope>();
		env->m_colour = Colours::aqua;
		env->m_name = "Bound A";
		env->add_node(EnvelopeNode(0.0, 0.1, NodeShape::Power,0.5));
		
		env->add_node(EnvelopeNode(1.0, 1.0, NodeShape::Power,0.5));
		m_envelopes.push_back(env);
		env = std::make_shared<Envelope>();
		env->m_colour = Colours::red;
		env->m_name = "Bound B";
		env->add_node(EnvelopeNode(0.0, 0.0,NodeShape::Power,0.5));
		
		env->add_node(EnvelopeNode(1.0, 0.9,NodeShape::Power,0.5));
		m_envelopes.push_back(env);
	}
	String name() const { return m_name; }
	std::vector<EnvelopeRef> m_envelopes;
	int m_pfield = 0;
private:
	String m_name;
};

using MaskRef = std::shared_ptr<TendencyMask>;

struct ScoreEvent
{
	int m_instrument = 1;
	std::vector<double> m_fields;
};

class Score
{
public:
	std::vector<ScoreEvent> m_events;
private:
	
};

using ScoreRef = std::shared_ptr<Score>;
