#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include "envelope.h"
#include <random>
#include <functional>

class MaskComponent : public Component
{
public:
	MaskComponent(MaskRef ref, ScoreRef sco) : m_mask(ref), m_score(sco)
	{
		
	}
	void paint(Graphics& g);
	void mouseDown(const MouseEvent& e);
	void mouseUp(const MouseEvent& e);
	void mouseMove(const MouseEvent& e);
	void mouseDrag(const MouseEvent& e);
	void mouseWheelMove(const MouseEvent &event, const MouseWheelDetails &wheel);
	std::function<void(TendencyMask*)> MaskChanged;
private:
	void draw_envelope(EnvelopeRef env, Graphics& g, Colour colour);
	MaskRef m_mask;
	ScoreRef m_score;
	std::mt19937 m_rand_gen;
	bool m_mouse_down = false;
	std::pair<EnvelopeRef, size_t> get_hode_node(int x, int y);
	std::pair<EnvelopeRef, size_t> get_hot_segment(int x, int y);
	std::pair<EnvelopeRef, size_t> m_hot_node;
	std::pair<EnvelopeRef, size_t> m_hot_segment;
	double m_r = 3.5;
	double m_p1_drag_start = 0.5;
};

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   : public Component
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void paint (Graphics&);
    void resized();
	
private:
	std::vector<std::shared_ptr<MaskComponent>> m_env_comps;
	std::vector<MaskRef> m_masks;
	ScoreRef m_score;
	//==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
