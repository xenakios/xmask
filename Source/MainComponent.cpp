/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

void MaskComponent::draw_envelope(EnvelopeRef env, Graphics& g, Colour colour)
{
	g.setColour(colour);
	const float nodesize = 5.0f;
	for (int i = 0; i < env->size()-1; ++i)
	{
		auto node0 = env->get_const_node(i);
		auto node1 = env->get_const_node(i+1);
		float xcor0 = node0.x()*getWidth();
		float ycor0 = getHeight() - getHeight()*node0.y();
		float xcor1 = node1.x()*getWidth();
		float ycor1 = getHeight() - getHeight()*node1.y();
		double value_delta = node1.y() - node0.y();
		int num_sub_segments = std::max((int)((node1.x() - node0.x())*getWidth() / 8), 8);
		for (int j = 0; j<num_sub_segments; ++j)
		{
			float offset0 = (xcor1 - xcor0) / num_sub_segments*j;
			float offset1 = (xcor1 - xcor0) / num_sub_segments*(j + 1);
			double to_shaping = 1.0 / (num_sub_segments)*j;
			double shaped = node0.y() + value_delta*get_shaped_value(to_shaping, node0.shape(), node0.p1(), 0.0);
			float ycor2 = (1.0 - shaped)*getHeight();
			to_shaping = 1.0 / num_sub_segments*(j + 1);
			shaped = node0.y() + value_delta*get_shaped_value(to_shaping, node0.shape(), node0.p1(), 0.0);
			float ycor3 = (1.0 - shaped)*getHeight();
			g.drawLine(xcor0 + offset0, ycor2, xcor0 + offset1, ycor3);
		}

		//g.drawLine(xcor0, ycor0, xcor1, ycor1, 1.5f);
		g.drawEllipse(xcor0-nodesize/2, ycor0-nodesize/2, nodesize, nodesize,1.0f);
		if (i==env->size()-2)
			g.drawEllipse(xcor1 - nodesize / 2, ycor1 - nodesize / 2, nodesize, nodesize, 1.0f);
	}
	
}

void MaskComponent::mouseDown(const MouseEvent& ev)
{
	m_mouse_down = true;
	auto hot = get_hode_node(ev.x, ev.y);
	if (hot.first == nullptr && ev.mods.isAltDown()==false)
	{
		double new_x = 1.0 / getWidth()*ev.x;
		double new_y = 1.0 - (1.0 / getHeight()*ev.y);
		double val_a = m_mask->m_envelopes[0]->interpolate(new_x);
		double val_b = m_mask->m_envelopes[1]->interpolate(new_x);
		double dist_a = fabs(val_a - new_y);
		double dist_b = fabs(val_b - new_y);
		if (dist_a < dist_b)
			m_mask->m_envelopes[0]->add_node(EnvelopeNode(new_x, new_y,NodeShape::Power,0.5));
		else m_mask->m_envelopes[1]->add_node(EnvelopeNode(new_x, new_y,NodeShape::Power,0.5));
		MaskChanged(m_mask.get());
		return;
	}
	if (hot.first != nullptr && ev.mods.isAltDown()==true)
	{
		hot.first->remove(hot.second);
		MaskChanged(m_mask.get());
		return;
	}
	auto hot_segment = get_hot_segment(ev.x, ev.y);
	if (hot_segment.first != nullptr)
		m_p1_drag_start = hot_segment.first->get_const_node(hot_segment.second).p1();
}

void MaskComponent::mouseUp(const MouseEvent&)
{
	m_mouse_down = false;
	m_hot_node = std::make_pair(nullptr, 0);
}

std::pair<EnvelopeRef, size_t> MaskComponent::get_hode_node(int x, int y)
{
	const double nodesize = 4.0;
	for (auto& e : m_mask->m_envelopes)
	{
		for (int i = 0; i < e->size(); ++i)
		{
			auto node = e->get_const_node(i);
			double xcor = getWidth()*node.x();
			double ycor = getHeight() - getHeight()*node.y();
			Rectangle<double> r(xcor-nodesize, ycor-nodesize, nodesize*2, nodesize*2);
			if (r.contains(x, y))
				return std::make_pair(e, i);
		}
	}
	return std::make_pair(nullptr, 0);
}

std::pair<EnvelopeRef, size_t> MaskComponent::get_hot_segment(int x, int y)
{
	const double nodesize = 4.0;
	for (auto& e : m_mask->m_envelopes)
	{
		double y_target = getHeight()-getHeight()*e->interpolate(1.0 / getWidth()*x);
		for (int i = 0; i < e->size()-1; ++i)
		{
			auto node0 = e->get_const_node(i);
			auto node1 = e->get_const_node(i+1);
			double xcor0 = getWidth()*node0.x();
			double ycor0 = getHeight() - getHeight()*node0.y();
			double xcor1 = getWidth()*node1.x();
			double ycor1 = getHeight() - getHeight()*node1.y();
			if (x >= xcor0 && x < xcor1 && y >= y_target - 5.0 && y < y_target + 5.0)
				return std::make_pair(e, i);
		}
	}
	return std::make_pair(nullptr, 0);
}

void MaskComponent::mouseMove(const MouseEvent& ev)
{
	auto hot = get_hode_node(ev.x, ev.y);
	if (hot.first != nullptr)
		setMouseCursor(MouseCursor::PointingHandCursor);
	else setMouseCursor(MouseCursor::NormalCursor);
	//if (m_mouse_down == false)
	m_hot_node = hot;
	if (m_hot_node.first == nullptr)
	{
		m_hot_segment = get_hot_segment(ev.x, ev.y);
		if (m_hot_segment.first != nullptr)
			setMouseCursor(MouseCursor::LeftRightResizeCursor);
		else setMouseCursor(MouseCursor::NormalCursor);
	}
}

void MaskComponent::mouseWheelMove(const MouseEvent &event, const MouseWheelDetails &wheel)
{
	m_r = bound_value(3.0, m_r + wheel.deltaY*0.01, 3.9999);
	//Logger::writeToLog(String(m_r));
	MaskChanged(m_mask.get());
}

void MaskComponent::mouseDrag(const MouseEvent& ev)
{
	if (m_hot_segment.first != nullptr && ev.mods.isAltDown()==true)
	{
		auto node = m_hot_segment.first->get_const_node(m_hot_segment.second);
		double newp1 = bound_value(0.0,m_p1_drag_start+ev.getDistanceFromDragStartX()*0.005,1.0);
		//Logger::writeToLog(String(newp1));
		auto oldshape = node.shape();
		m_hot_segment.first->set_node(m_hot_segment.second, EnvelopeNode(node.x(), node.y(), oldshape, newp1));
		MaskChanged(m_mask.get());
		return;
	}
	if (m_hot_node.first != nullptr)
	{
		//if (m_hot_node.first != nullptr)
		{
			double new_x = 1.0 / getWidth() * ev.x;
			new_x = bound_value(0.0, new_x, 1.0);
			if (m_hot_node.second>0 && m_hot_node.second<m_hot_node.first->size() - 1)
				new_x = bound_value(m_hot_node.first->get_const_node(m_hot_node.second-1).x() + 0.001,
				new_x,
				m_hot_node.first->get_const_node(m_hot_node.second + 1).x() - 0.001 );

			double new_y = 1.0 - (1.0 / getHeight()*ev.y);
			new_y = bound_value(0.0, new_y, 1.0);
			NodeShape old_shape = m_hot_node.first->get_const_node(m_hot_node.second).shape();
			double old_p1 = m_hot_node.first->get_const_node(m_hot_node.second).p1();
			m_hot_node.first->set_node(m_hot_node.second, EnvelopeNode(new_x, new_y,old_shape,old_p1));
			MaskChanged(m_mask.get());
		}

	}
}

void MaskComponent::paint(Graphics& g)
{
	g.fillAll(Colours::black);
	g.setColour(Colours::yellow);
	g.drawText(m_mask->name()+ " "+String((int)m_score->m_events.size())+" events", 10, 10, getWidth(), getHeight(), Justification::topLeft);
	if (m_hot_node.first != nullptr)
		g.drawText(m_hot_node.first->m_name+" "+String((int)m_hot_node.second), 10, 40, getWidth(), getHeight(), Justification::topLeft);
	for (auto& e : m_mask->m_envelopes)
		draw_envelope(e, g, e->m_colour);
	g.setColour(Colours::yellow);
	const int num_points = m_score->m_events.size();
	m_rand_gen = std::mt19937(1);
	logistic_sequence gen;
	gen.m_r = m_r;
	if (m_mask->m_pfield == 0)
		return;
	for (int i = 0; i < num_points; ++i)
	{
		double norm_x = m_score->m_events[i].m_fields[0];
		/*
		double lim_a = m_mask->m_envelopes[1]->interpolate(norm_x);
		double lim_b = m_mask->m_envelopes[0]->interpolate(norm_x);
		if (lim_b < lim_a)
			std::swap(lim_a, lim_b);
		//std::cauchy_distribution<double> dist(0.5, 0.01);
		std::_Beta_distribution<double> dist(0.4, 0.4);
		double value = gen(); // dist(m_rand_gen);
		value = 0.5+0.5*sin(norm_x*3.141592653*128.0);
		value = bound_value(0.0,value,1.0);
		value = lim_a + (lim_b - lim_a) * value;
		*/
		double value = m_score->m_events[i].m_fields[m_mask->m_pfield];
		double xcor = getWidth()*norm_x;
		double ycor = (double)getHeight() - getHeight()*value;
		g.setPixel(xcor, ycor);
	}
}

//==============================================================================
MainContentComponent::MainContentComponent()
{
	m_score = std::make_shared<Score>();
	for (int i = 0; i < 4; ++i)
		m_masks.push_back(std::make_shared < TendencyMask> ("P " + String(i + 1), i));
	for (int i = 0; i < 4; ++i)
	{
		MaskRef mask = m_masks[i];
		m_env_comps.push_back(std::make_shared<MaskComponent>(mask,m_score));
		m_env_comps.back()->MaskChanged = [this,i](TendencyMask*)
		{
			int num_events = 1000;
			std::mt19937 gen(1);
			m_score->m_events.clear();
			//m_score->m_events.reserve(num_events);
			double t = 0.0;
			int sanity = 0;
			while (t<1.0)
			{
				ScoreEvent ev;
				ev.m_fields.resize(m_masks.size());
				ev.m_fields[0] = t;
				for (int k = 1; k < m_masks.size(); ++k)
				{
					double lim_a = m_masks[k]->m_envelopes[1]->interpolate(t);
					double lim_b = m_masks[k]->m_envelopes[0]->interpolate(t);
					if (lim_b < lim_a)
						std::swap(lim_a, lim_b);
					std::uniform_real_distribution<double> dist(0.0, 1.0);
					double value = dist(gen); // 0.5 + 0.5*sin(t*3.141592653*128.0);
					value = bound_value(0.0, value, 1.0);
					value = lim_a + (lim_b - lim_a) * value;
					ev.m_fields[k] = value;
				}
				
				m_score->m_events.push_back(ev);
				//double lambda = 100.0+4900.0*m_masks[0]->m_envelopes[0]->interpolate(t);
				std::exponential_distribution<double> tdist(500.0);
				double lim_a = 0.01*m_masks[0]->m_envelopes[1]->interpolate(t);
				double lim_b = 0.01*m_masks[0]->m_envelopes[0]->interpolate(t);
				if (lim_b < lim_a)
					std::swap(lim_a, lim_b);
				double value = tdist(gen);
				value = bound_value(0.0, value, 1.0);
				value = lim_a + (lim_b - lim_a) * value;
				t += value;
				++sanity;
				if (sanity >= 10000)
					break;
			}
			
			for (auto& e : m_env_comps)
				e->repaint();
		};
		addAndMakeVisible(m_env_comps.back().get());
	}
	
	setSize(1000, 700);
	
}

MainContentComponent::~MainContentComponent()
{
}

void MainContentComponent::paint (Graphics& g)
{
	g.fillAll(Colours::grey);
	return;
	g.fillAll (Colour (0xff001F36));

    g.setFont (Font (16.0f));
    g.setColour (Colours::white);
    g.drawText ("Hello World!", getLocalBounds(), Justification::centred, true);
}

void MainContentComponent::resized()
{
	int avail_width = getWidth() - 10;
	int avail_height = getHeight() - 10;
	int grid_cols = 2;
	int grid_rows = 2;
	for (int i = 0; i < m_env_comps.size(); ++i)
	{
		int xpos = i % grid_cols;
		int ypos = i / grid_rows;
		m_env_comps[i]->setBounds(10+xpos*(avail_width/grid_cols), 
								  10+ypos*(avail_height/grid_rows), 
								  avail_width/grid_cols - 20, 
								  avail_height/grid_rows - 20);
	}
}
